package ru.t1.nikitushkina.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.nikitushkina.tm.api.repository.model.ISessionRepository;
import ru.t1.nikitushkina.tm.api.service.model.ISessionService;
import ru.t1.nikitushkina.tm.model.Session;

@Service
@NoArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}
