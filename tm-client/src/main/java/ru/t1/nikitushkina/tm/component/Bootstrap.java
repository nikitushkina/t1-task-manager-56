package ru.t1.nikitushkina.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.api.endpoint.*;
import ru.t1.nikitushkina.tm.api.service.*;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.nikitushkina.tm.exception.system.CommandNotSupportedException;
import ru.t1.nikitushkina.tm.util.SystemUtil;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.nikitushkina.tm.command";
    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;
    @Getter
    @NotNull
    @Autowired
    private AbstractCommand[] abstractCommands;
    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;
    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;
    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;
    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpointClient;
    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpointClient;
    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpointClient;
    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpointClient;
    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpointClient;

    private void exitApplication() {
        System.exit(0);
    }

    private void initCommands(@NotNull final AbstractCommand[] commands) {
        for (@NotNull AbstractCommand command : commands) {
            registry(command);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void prepareStartup() {
        initPID();
        initCommands(abstractCommands);
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        try {
            processArgument(arg);
        } catch (@NotNull final Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nENTER COMMAND: ");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) exitApplication();
        prepareStartup();
        processCommands();
    }

}
