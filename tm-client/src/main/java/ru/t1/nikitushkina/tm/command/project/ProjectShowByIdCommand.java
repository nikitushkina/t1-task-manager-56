package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;
import ru.t1.nikitushkina.tm.dto.request.ProjectShowByIdRequest;
import ru.t1.nikitushkina.tm.dto.response.ProjectShowByIdResponse;
import ru.t1.nikitushkina.tm.util.TerminalUtil;


@Component
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setProjectId(id);
        @Nullable final ProjectShowByIdResponse response = projectEndpoint.showById(request);
        @Nullable final ProjectDTO project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
