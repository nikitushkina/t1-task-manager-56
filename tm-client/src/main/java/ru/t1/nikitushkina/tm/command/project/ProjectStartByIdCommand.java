package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.dto.request.ProjectStartByIdRequest;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start project by id.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken());
        request.setProjectId(id);
        projectEndpoint.startById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
